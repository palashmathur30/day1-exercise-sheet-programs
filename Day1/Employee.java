import java.util.Comparator;

public class Employee //implements Comparable<Employee>
{

	private int employeeID;
	private String employeeName;
	private int mathMarks;

	public Employee(int employeeID, String employeeName, int mathMarks) {
		this.employeeID = employeeID;
		this.employeeName = employeeName;
		this.mathMarks = mathMarks;
	}

	public int getemployeeID() {
		return employeeID;
	}

	public String getemployeeName() {
		return employeeName;
	}

	public int getmathMarks() {
		return mathMarks;
	}
//	public int compareTo(Employee e)
//	{
//		return e.getemployeeID() - this.getemployeeID();
//	}

	@Override
	public String toString() {
		return "Employee [employeeID=" + employeeID + ", employeeName=" + employeeName + ", mathMarks=" + mathMarks + "]";
	}
	
}



class MyCamparator implements Comparator<Employee>
{
	public int compare(Employee e1,Employee e2)
	{
		if(e1.getmathMarks() == e2.getmathMarks()) 
		{
			return e1.getemployeeID() - e2.getemployeeID();
		}
		return e1.getmathMarks() - e2.getmathMarks();
	}
}

