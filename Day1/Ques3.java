class Cat
{
	public void walk()
	{
		System.out.println("class A walk method");
	}
}
class Dog extends Cat
{
	public void run()
	{
		System.out.println("class B run method");
	}
}

public class Ques3 {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Dog obj = new Dog();
		obj.walk();
		obj.run();
		
	}

}
