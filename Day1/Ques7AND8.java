class Circle
{
	private double radius;
	private double area;
	
	public double getRadius() {
		return radius;
	}
	public void setRadius(double radius) {
		this.radius = radius;
	}
	public double getArea() {
		return area;
	}
	public void setArea(double area) {
		this.area = area;
	}
}

public class Ques7AND8 {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Circle c1 = new Circle();
		
		c1.setRadius(10.00);
		System.out.println(c1.getRadius());
		
		c1.setArea(20.00);
		System.out.println(c1.getArea());
		
	}

}
